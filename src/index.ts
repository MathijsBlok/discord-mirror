import * as dotenv from 'dotenv';
import {MongoClient} from 'mongodb';
import {filter, tap} from 'rxjs/operators';
import {DiscordClient} from 'rx-discord-client';

dotenv.config();

async function saveMessage(data: any) {
    const client = new MongoClient(`mongodb://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOST}:27017`);
    try {
        await client.connect();
        const result = await client
            .db(process.env.MONGO_DATABASE)
            .collection('messages')
            .insertOne(data);
        console.log(`[${new Date().toISOString()}] `, `Message saved with id [${result.insertedId}]`);
    } catch (e) {
        console.log(`[${new Date().toISOString()}] `, `Error saving message [${e.toString()}]`);
    } finally {
        await client.close();
    }
}

DiscordClient.create(process.env.TYPE_REQUEST_TOKEN)
    .pipe(
        tap(msg => console.log(`[${new Date().toISOString()}] `, `Received message of type [${msg.type}]`)),
        filter(msg => msg.type === 'MESSAGE_CREATE')
    )
    .subscribe((msg) => saveMessage(msg.data))
