# Discord server mirror
#### Project to save all messages from discord server to mongoDB

## Prerequisites

* Have a working version of docker and docker-compose on the targeted server/pc ([read more about how to install docker](https://docs.docker.com/get-docker/)) 

## How to run

Create .env file in the root of this project with the following contents (you can of course change the username, password and database to your liking)

```text
TYPE_REQUEST_TOKEN=<*discord authorization token>
MONGO_USERNAME="root"
MONGO_PASSWORD="rootpassword"
MONGO_DATABASE="discord-mirror"
```
You can obtain the discord authorization token by following [these instructions](https://www.youtube.com/watch?v=b9agj9jyNnI&ab_channel=Exordium)

Then run the project with 
```bash
docker-compose up -d
```
